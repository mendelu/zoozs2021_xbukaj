#include <iostream>
using namespace std;

class Sportovec{
public:
    string m_jmeno;
    float m_celkomKM;

    void  setJmeno(string noveJmeno){
        m_jmeno = noveJmeno;
    }
    void  behaj(float kolkoKM){
        m_celkomKM = m_celkomKM + kolkoKM;
    }
    string getJmeno ()
    {
        return m_jmeno;
    }
    void printInfo(){
        cout << "Jméno: " << m_jmeno << "\n";
        cout << "Celkom km: " << m_celkomKM;
    }

};

int main() {
    Sportovec* sportovec1 = new Sportovec();
    sportovec1->setJmeno("Jarda");
    sportovec1->behaj(25);
    sportovec1->behaj(2);
    sportovec1->printInfo();

    return 0;
}
