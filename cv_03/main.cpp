#include <iostream>

using namespace std;

class Programator {
    string m_jmeno;
    int m_hodinovaMzda;
    int m_pocetOdpracovanychHodin = 0;
    int m_bonusZaHodinuPrescasu;
    int m_pocetChyb = 0;
    int m_srazkaZaCHybu = 5;
    int m_srazka=0;
public:
    Programator(string jmeno, int hodinovaMzda, int bonusZaHodinuPrescasu) {
        m_jmeno = jmeno;
        m_hodinovaMzda = hodinovaMzda;
        m_bonusZaHodinuPrescasu = bonusZaHodinuPrescasu;
    };

    Programator(string jmeno) {
        m_jmeno = jmeno;
        m_hodinovaMzda = 150;
        m_bonusZaHodinuPrescasu = 200;
    };

    void setPracuj(int kolikHodin) {
        m_pocetOdpracovanychHodin = kolikHodin + m_pocetOdpracovanychHodin;
    };

    void konecMesice() {
        m_pocetOdpracovanychHodin = 0;
    };


    void setChyby(int chyby) {
        m_pocetChyb=chyby;
        if(m_pocetChyb >= 0) {
            m_pocetChyb = chyby + m_pocetChyb;
        }
        else
        {
            cout<<"Pocet chyby nemuze byt mensi jak nula\n"
                  "hodnota bude nastavena na 0"<<endl;
            m_pocetChyb=0;
        }
    };

private:
    int vypocitejMzdu() {
        int mzda;
        if (m_pocetChyb <= 0) {
            mzda = m_pocetOdpracovanychHodin * m_hodinovaMzda + vypocitejBonusKMzde();
        } else {
            mzda = (m_pocetOdpracovanychHodin * m_hodinovaMzda +
                    vypocitejBonusKMzde()) - evidujChyby();
        }
        return mzda;
    };

    int evidujChyby(){
        m_srazka=m_pocetChyb*m_srazkaZaCHybu;
    };

    int vypocitejBonusKMzde() {
        if (m_pocetOdpracovanychHodin >= 40) {
            return (m_pocetOdpracovanychHodin - 40) * m_bonusZaHodinuPrescasu;
        }
        return 0;

    };

public:
    void printInfo() {
        cout << "Jmeno: " << m_jmeno << endl;
        cout << "Mzda za hod: " << m_hodinovaMzda << endl;
        cout << "Bonus za prescas: " << m_bonusZaHodinuPrescasu << endl;
        cout << "Pocet hodin: " << m_pocetOdpracovanychHodin << endl;
        cout << "Mzda: " << vypocitejMzdu() << endl;
        cout << "Pocet chyb: " << m_pocetChyb << endl;
        cout << "--------------" << endl;
    }
};

int main() {
    Programator* p1 = new Programator("Lojza", 120, 150);
    Programator* p2=new Programator("Pepa");
    p2->printInfo();
    p1->setPracuj(20);
    p1->printInfo();
    p1->setPracuj(21);
    p1->printInfo();
    p1->setChyby(2);
    p1->printInfo();
    p1->setChyby(8);
    p1->printInfo();
    p1->konecMesice();
    p1->setChyby(-11);
    p1->printInfo();
    delete p1;
    return 0;
}
