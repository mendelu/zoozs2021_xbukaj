#include <iostream>
#include <array>
using namespace std;

class Armor{
    int m_defenceBonus;
public:
    Armor(int defenceBonus){
        m_defenceBonus = defenceBonus;
    }

    int getDefenceBonus(){
        return m_defenceBonus;
    }
};

class Knight{
    int m_defence;
    array<Armor*, 5> m_equippedArmor;

public:
    Knight(int defence){
        m_defence = defence;
        for (int i=0; i<m_equippedArmor.size(); ++i){
            m_equippedArmor.at(i) = nullptr;
        }
    }

    void equipArmor(Armor* armor, int slot){
        if (m_equippedArmor.at(slot) == nullptr){
            m_equippedArmor.at(slot) = armor;
        } else {
            cout << "Cannot equip the new armor" << endl;
        }
    }

    void dropArmor(int slot){
        m_equippedArmor.at(slot) = nullptr;
    }

    int getDefence(){
        /*
        if (m_equippedArmor == nullptr){
          return m_defence;
        } else {
          return m_defence + m_equippedArmor->getDefenceBonus();
        }
        */
    }
};

int main() {
    Armor* chainMail = new Armor(10);
    Armor* paperMail = new Armor(1);

    Knight* lancelot = new Knight(100);

    cout << "Lancelot: " << lancelot->getDefence() << endl;
    lancelot->equipArmor(chainMail);
    cout << "Lancelot: " << lancelot->getDefence() << endl;


    lancelot->dropArmor(chainMail);
    cout << "Lancelot: " << lancelot->getDefence() << endl;
    lancelot->equipArmor(paperMail);
    cout << "Lancelot: " << lancelot->getDefence() << endl;

    delete paperMail;
    delete chainMail;
    delete lancelot;
    return 0;
}