//
// Created by xjakubek on 04.11.2021.
//

#ifndef CV_08_DIAGNOSIS_H
#define CV_08_DIAGNOSIS_H

#include <string>

class Diagnosis {
    std::string m_description;

public:
    Diagnosis(std::string description);
};


#endif //CV_08_DIAGNOSIS_H
