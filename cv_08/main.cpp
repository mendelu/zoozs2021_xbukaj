#include <iostream>

#include "System.h"

int main() {
    System::createPatient("Tomas");
    System::createPatient("Denisa");
    System::createPatient("Honza");

//    auto patient=System::findPatient(2);
//    patient->printInfo();
    auto *dgs1=new Diagnosis("Kaz");
    System::enterNewDiagnoses({dgs1},2,1,3);
//    patient->printInfo();
    System::enterNewDiagnoses({dgs1},2,1,3);
//    patient->printInfo();
//    auto patient2=System::findPatient(1);
//    patient2->printInfo();
    System*sys=new System();
    sys->vypisPacienty();
    return 0;
}
