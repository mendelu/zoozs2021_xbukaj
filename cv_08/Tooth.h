//
// Created by xjakubek on 04.11.2021.
//

#ifndef CV_08_TOOTH_H
#define CV_08_TOOTH_H

#include "Diagnosis.h"
#include <vector>

class Tooth {
    std::string m_type;
    std::vector<Diagnosis *> m_diagnoses;

public:
    Tooth(std::string type);

    std::vector<Diagnosis *> getDiagnoses();

    void addDiagnoses(std::vector<Diagnosis *> newDgs);
};


#endif //CV_08_TOOTH_H
