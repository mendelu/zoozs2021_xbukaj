//
// Created by xjakubek on 04.11.2021.
//

#include "System.h"

std::vector<Patient *> System::s_patients = {};
int System::s_patientsCount = 0;

void System::createPatient(std::string name) {
    Patient *newPatient = new Patient(name, s_patientsCount);
    s_patientsCount++;
    s_patients.push_back(newPatient);
}

Patient *System::findPatient(int id) {
    Patient *foundPatient = nullptr;

    for (auto *patient:s_patients) {
        if (patient->getId() == id) {
            foundPatient = patient;
        }
    }

    if (foundPatient == nullptr) {
        std::cout << "Patient with ID " << id << " was not found!" << std::endl;
    }

    return foundPatient;
}
void System::vypisPacienty(){
        for (auto *patient:s_patients) {
                patient->printInfo();
        }
}
void System::enterNewDiagnoses(std::vector<Diagnosis *> diagnoses, int patientId, int jawId, int toothId) {
    Patient*foundPatient= findPatient(patientId);
    if(foundPatient == nullptr){
        return;
    }
    auto teeth=foundPatient->getTeeth();
    if(teeth[jawId][toothId]== nullptr){
        std::cout<<"Tooth doesn´t exist"<<std::endl;
        return;
    }
    teeth[jawId][toothId]->addDiagnoses(diagnoses);
    std::cout<<"Diagnoses saved"<<std::endl;
}