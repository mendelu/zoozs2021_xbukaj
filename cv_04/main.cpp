#include <iostream>
using namespace std;

class ErrorLog{
private:
    string m_errors;
    static ErrorLog* s_log;
public:
    void logError(string what, string where){
        m_errors += "- " + where + ": " + what +"\n";
    }

    string getErrors(){
        return m_errors;
    }

    static ErrorLog* getErrorLog(){
        if(s_log== nullptr){
            s_log= new ErrorLog();
        }
        return s_log;
    }

private:
    ErrorLog(){
        m_errors="ERROR LOGS: \n";
    }
};
ErrorLog* ErrorLog::s_log = nullptr;
class Drak{
    float m_zdravi;
    float m_sila;
    float m_obrana;
public:
    Drak(float sila, float obrana){
        m_zdravi=100;
        setSila(sila);
        setObrana(obrana);
    }

    float getZdravi(){
        return m_zdravi;
    }

    float getSila(){
        return m_sila;
    }

    float getObrana(){
        return m_obrana;
    }

    void snizZdravi(float kolikZdravi){
        m_zdravi -= kolikZdravi;
    }
    void printInfo(){
        cout<<"Zdravi draka: "<<m_zdravi<<endl;
        cout<<"Sila draka: "<<m_sila<<" Obrana draka: "<<m_obrana<<endl;
        cout<<"----------------------------------"<<endl;
    }

private:
    void setSila(float sila){
        if (sila<=100){
            m_sila=sila;
        }
        else{
            ErrorLog *log = ErrorLog::getErrorLog();
            log->logError("Nespravna sila","Trida drak");
            m_sila=0;
        }
    }
    void setObrana(float obrana){
        if ((obrana<=200)&&(obrana>=0)){
            m_obrana=obrana;
        }
        else{
            ErrorLog *log = ErrorLog::getErrorLog();
            log->logError("Nespravna obrana","Trida drak");
            m_obrana=0;
        }
    }
};

class Rytir{
    float m_zdravi;
    float m_sila;
    float m_obrana;
    string m_jmeno;
public:
    Rytir(float sila, float obrana,string jmeno){
        m_jmeno=jmeno;
        m_zdravi=100;
        setSila(sila);
        setObrana(obrana);
    }

    float getZdravi(){
        return m_zdravi;
    }

    float getSila(){
        return m_sila;
    }

    float getObrana(){
        return m_obrana;
    }

    string getJmeno(){
        return m_jmeno;
    }

    void zautoc(Drak* nepritel){
        float silaNepritele = nepritel ->getSila();
        float obranaNepritele = nepritel->getObrana();
        //kontrola , zda je rytir>drak
        if(m_sila>obranaNepritele){
            //Rytir vyhral souboj
            nepritel->snizZdravi(m_sila-obranaNepritele);
        }
        //kontrola, zda je drak>rytir
        else{
            //Drak vyhral souboj
            m_zdravi-=silaNepritele - m_obrana;
        }
    }

    void printInfo(){
        cout<<"Jmeno hrdiny: "<< m_jmeno<< endl;
        cout<<"Zdravi hrdiny: "<<m_zdravi<<endl;
        cout<<"Sila hrdiny: "<<m_sila<<" Obrana hrdiny: "<<m_obrana<<endl;
        cout<<"----------------------------------"<<endl;
    }
private:
    void setSila(float sila){
        if (sila<=100){
            m_sila=sila;
        }
        else{
            ErrorLog *log = ErrorLog::getErrorLog();
            log->logError("Nespravna sila","Trida rytir");
            m_sila=0;
        }
    }
    void setObrana(float obrana){
        if ((obrana<=200)&&(obrana>=0)){
            m_obrana=obrana;
        }
        else{
            ErrorLog *log = ErrorLog::getErrorLog();
            log->logError("Nespravna obrana","Trida rytir");
            m_obrana=0;
        }
    }
};
int main() {

    Drak* drak = new Drak(20,10);
    Rytir* rytir=new Rytir(101,15,"Macenka z Arku");
    rytir->printInfo();
    ErrorLog *log = ErrorLog::getErrorLog();
    cout << log->getErrors();
    rytir->zautoc(drak);
    rytir->getSila();
    rytir->printInfo();
    return 0;
}
