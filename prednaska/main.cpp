#include <iostream>
using namespace std;

class Lektvar{
    int m_mana;
public:
    Lektvar(int mana){
        m_mana=mana;
    }

    int getMana(){
        return m_mana;
    }
};

class Svitek{
    string m_kouzlo;
    int m_spotrebaMany;

public:
    Svitek(string kouzlo, int spotrebaMany){
        m_kouzlo=kouzlo;
        m_spotrebaMany = spotrebaMany;
    }
    string getKouzlo(){
        return m_kouzlo;
    }
    int getPotrebnaMana(){
        return m_spotrebaMany;
    }
};

class Kouzelnik{
    string m_jmeno;
    int m_mana;
    string m_kouzlo;
    int m_kolikManyPotreba;

public:
    Kouzelnik(string jmeno){
        m_jmeno = jmeno;
        m_kouzlo="";
        m_kolikManyPotreba=0;
        m_mana=0;
    }

    void vypijLektvar(Lektvar*lektvar){
        m_mana += lektvar->getMana();
    }
    void naucSeKouzlo(Svitek*svitek){
        m_kouzlo = svitek->getKouzlo();
        m_kolikManyPotreba = svitek->getPotrebnaMana();
    }
    void kouzli(){
        if(m_kolikManyPotreba<= m_mana){
            cout<<"Ja "<< m_jmeno<<" , kouzlim kouzlo: ";
            cout<<m_kouzlo<<endl;
            m_mana = m_mana - m_kolikManyPotreba;
        } else
        {
            cout<<"Nedostatek many"<<endl;
        }
    }
};
int main() {
    Lektvar* modry = new Lektvar(10);
    Svitek* ozivovak = new Svitek("Ozij",30);
    Kouzelnik* lojza = new Kouzelnik("Lojza");

    lojza->vypijLektvar(modry);
    lojza->vypijLektvar(modry);
    lojza->vypijLektvar(modry);
    lojza->naucSeKouzlo(ozivovak);
    lojza->kouzli();
    lojza->kouzli();

    return 0;
}
