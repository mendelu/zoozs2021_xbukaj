#include <iostream>
using namespace std;

int main() {
class Jidlo {
    int m_cena;
    int m_vaha = 300;
    string m_nazev="";
public:
    Jidlo(string nazev,int vaha, int cena){
        m_nazev=nazev;
        m_vaha=vaha;
        m_cena=cena;
    }
    Jidlo(string nazev, int cena){
        m_nazev=nazev;
        m_cena=cena;
    }

    int getCena(){
        return  m_cena;
    }
    string getNazev(){
        return  m_nazev;
    }
};

class Student{
    int m_id;
    int m_kredit=0;
    string m_seznamVeci;
    int m_celkovaCena=0;
public:
    Student(int ID, int kredit){
        m_id=ID;
        m_kredit=kredit;
    }

    void zvysKredit(int kredit){
        m_kredit+=kredit;
    }
    int getKredit(){
        return m_kredit;
    }
    int getCelkovaCena(){
        return m_celkovaCena;
    }

    void pridejJidlo(Jidlo*jidlo){
        m_seznamVeci+="Jidlo: "+jidlo->getNazev()+"\n";
        m_celkovaCena+=jidlo->getCena();
    }

    void zaplat(){
        if(m_kredit>m_celkovaCena){
            m_kredit-=m_celkovaCena;
        }
        else{
            cout<<"**********************"<<endl;
            cout<<"Nemas dostatek kreditu"<<endl;
            cout<<"**********************"<<endl;
        }
    }

    void printInfo(){
        cout<<"Student: "<<m_id<<endl;
        cout<<"Kredit: "<<m_kredit<<endl;
        cout<<"Seznam jidel: "<<endl<<m_seznamVeci<<endl;
        cout<<"Celkova utrata: "<<m_celkovaCena<<endl;
        cout<<"--------------------------------------"<<endl;
    }
};

    Student*s1=new Student(1,200);
    Jidlo*j1=new Jidlo("Rizek s hranolkama",120);
    Jidlo*j2=new Jidlo("Spenat s vajickem",115);
    Jidlo*j3=new Jidlo("Smazeny syr s bramborem",95);

    s1->pridejJidlo(j1);
    s1->pridejJidlo(j3);
    s1->zaplat();
    s1->printInfo();
    s1->zvysKredit(200);
    s1->zaplat();
    s1->printInfo();
    return 0;
}
