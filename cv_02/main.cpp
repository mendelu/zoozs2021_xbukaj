#include <iostream>
using namespace std;

class Balik{
public:
    float m_vaha;
    string m_prijemce;
    string m_odesilatel;
    int m_dobirka;

    //konstruktory muzou mit stejny nazev
    // pozor na poradi hodnot, treba prohodit

    Balik(string prijemce,string odesilatel,float vaha, int dobirka){
        m_prijemce=prijemce;
        m_odesilatel=odesilatel;
        m_vaha=vaha;
        m_dobirka=dobirka;
    };
    Balik(string prijemce,string odesilatel, float vaha) :
            Balik(prijemce, odesilatel,0.0,0){};


    Balik(string prijemce,string odesilatel, int dobirka):
            Balik(prijemce,odesilatel,0.0,dobirka){};

     void setVaha(float novaVaha){
         if(novaVaha < 0) {
             m_vaha = 0;
             cout << "zadal si nevalidni vahu!!!" << endl;
         }
         else
         {
             m_vaha=novaVaha;
         }

     };

     void nahradniPrijemce(){
        m_prijemce=m_odesilatel;
    }

     void printInfo(){
         cout<<"Prijemce: "<<m_prijemce<<endl;
         cout<<"Odesilatel: "<<m_odesilatel<<endl;
         cout<<"Dobirka: "<<m_dobirka<<endl;
         cout<<"Vaha: "<<m_vaha<<endl;
         cout<<"---------------------"<<endl;
         cout<<"---------------------"<<endl;
     }

};

int main() {
    Balik* kuryr = new Balik("Tomas","Zuzana",5000);
    Balik* posta=new Balik("Lukas","Maruska",10,500);
    kuryr->printInfo();
    posta->printInfo();
    kuryr->setVaha(20);
    kuryr->printInfo();
    return 0;
}
