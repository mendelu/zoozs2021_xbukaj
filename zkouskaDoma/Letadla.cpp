//
// Created by lukib on 07.01.2022.
//

#include "Letadla.h"

Letadla::Letadla(float spotreba ,std::string oznaceni) {
    m_oznaceni=oznaceni;
    m_spotreba=spotreba;
}
Letadla::Letadla(std::string oznaceni, float spotreba, int zavadlovyProstor) {
    m_oznaceni=oznaceni;
    m_spotreba=spotreba;
    m_zavazadlovyProstor=zavadlovyProstor;
}

Letadla::Letadla(std::string oznaceni, int pocetPasazeru, float spotreba) {
    m_oznaceni=oznaceni;
    m_spotreba=spotreba;
    m_pocetPasazeru=pocetPasazeru;
}

std::string Letadla::getOznaceni() {
    return m_oznaceni;
}

int Letadla::getSpotreba() {
    return m_spotreba;
}

int Letadla::getZavazProstor() {
    return m_zavazadlovyProstor;
}



