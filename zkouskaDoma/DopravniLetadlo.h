//
// Created by lukib on 07.01.2022.
//

#ifndef ZKOUSKADOMA_DOPRAVNILETADLO_H
#define ZKOUSKADOMA_DOPRAVNILETADLO_H
#include "Letadla.h"

class DopravniLetadlo:public Letadla {
public:
    DopravniLetadlo(std::string o,int p,float s): Letadla(o,p,s){};
    void printInfo();
    int getSpotreba();
};


#endif //ZKOUSKADOMA_DOPRAVNILETADLO_H
