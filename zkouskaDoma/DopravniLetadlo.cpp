//
// Created by lukib on 07.01.2022.
//

#include "DopravniLetadlo.h"

int DopravniLetadlo::getSpotreba() {
    m_spotreba=(m_pocetPasazeru*90)/m_spotreba;
    return m_spotreba;
}
void DopravniLetadlo::printInfo() {
    std::cout<<"Oznaceni: "<<m_oznaceni<<std::endl;
    std::cout<<"Spotreba: "<<getSpotreba()<<std::endl;
    std::cout<<"Pocet pasazeru: "<<m_pocetPasazeru<<std::endl;
    std::cout<<"------------------------"<<std::endl;
}
