//
// Created by lukib on 07.01.2022.
//

#ifndef ZKOUSKADOMA_NAKLADNILETADLO_H
#define ZKOUSKADOMA_NAKLADNILETADLO_H
#include "Letadla.h"

class NakladniLetadlo :public Letadla{
public:
    NakladniLetadlo(std::string o,int p,float s): Letadla(o,s,p){};
    void printInfo();
    int getSpotreba();
};


#endif //ZKOUSKADOMA_NAKLADNILETADLO_H
