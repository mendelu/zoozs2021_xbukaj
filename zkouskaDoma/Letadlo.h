//
// Created by lukib on 07.01.2022.
//

#ifndef ZKOUSKADOMA_LETADLO_H
#define ZKOUSKADOMA_LETADLO_H
#include "Letadla.h"

class Letadlo :public Letadla{
public:
    Letadlo(float s,std::string o): Letadla(s,o){};
    void printInfo();
};


#endif //ZKOUSKADOMA_LETADLO_H
