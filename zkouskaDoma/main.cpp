#include <iostream>
#include "Letadla.h"
#include "NakladniLetadlo.h"
#include "Letadlo.h"
#include "DopravniLetadlo.h"
#include "Letiste.h"

int main() {
    Letadlo*letadlo=new Letadlo(50,"ABC");
    NakladniLetadlo*nakladniLetadlo=new NakladniLetadlo("BCD",50,80);
    DopravniLetadlo*dopravniLetadlo=new DopravniLetadlo("CDE",50,100);
    letadlo->printInfo();
    nakladniLetadlo->printInfo();
    dopravniLetadlo->printInfo();

    Letiste*letiste=new Letiste("Brno",5);
    letiste->pridejLetadlo(letadlo);
    letiste->pridejLetadlo(nakladniLetadlo);
    letiste->pridejLetadlo(nakladniLetadlo);
    letiste->pridejLetadlo(dopravniLetadlo);
    letiste->pridejLetadlo(nakladniLetadlo);
    letiste->pridejLetadlo(letadlo);
    letiste->printInfo();
    return 0;
}
