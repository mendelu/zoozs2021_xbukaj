//
// Created by lukib on 07.01.2022.
//

#ifndef ZKOUSKADOMA_LETISTE_H
#define ZKOUSKADOMA_LETISTE_H
#include "vector"
#include "Letadla.h"

class Letiste {
    static std::vector<Letadla*>m_seznamLetadel;
    int m_kapacitaLetiste;
    std::string m_nazev;
public:
    Letiste(std::string nazev,int kapacita);
    void pridejLetadlo(Letadla*letadla);
    void oddelejLetadlo(int pozice);
    void printInfo();
};


#endif //ZKOUSKADOMA_LETISTE_H
