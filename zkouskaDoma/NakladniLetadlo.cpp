//
// Created by lukib on 07.01.2022.
//

#include "NakladniLetadlo.h"

void NakladniLetadlo::printInfo() {
    std::cout<<"Oznaceni: "<<m_oznaceni<<std::endl;
    std::cout<<"Spotreba: "<<getSpotreba()<<std::endl;
    std::cout<<"Zavazadlovy prostor: "<<m_zavazadlovyProstor<<std::endl;
    std::cout<<"------------------------"<<std::endl;
}

int NakladniLetadlo::getSpotreba() {
    m_spotreba=m_spotreba*(m_zavazadlovyProstor/30);
    return m_spotreba;
}
