//
// Created by lukib on 07.01.2022.
//

#ifndef ZKOUSKADOMA_LETADLA_H
#define ZKOUSKADOMA_LETADLA_H
#include <iostream>

class Letadla {
protected:
std::string m_oznaceni;
float m_spotreba;
int m_zavazadlovyProstor;
int m_pocetPasazeru;
public:
    Letadla(float spotreba ,std::string oznaceni);
    Letadla(std::string oznaceni, float spotreba,int zavadlovyProstor);
    Letadla(std::string oznaceni, int pocetPasazeru,float spotreba);
    std::string getOznaceni();
    int getSpotreba();
    int getZavazProstor();
};


#endif //ZKOUSKADOMA_LETADLA_H
